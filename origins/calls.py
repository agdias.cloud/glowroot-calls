#!/usr/bin/env python3
import json
import requests
import sys
from datetime import datetime, timedelta

NANOS_IN_ONE_MS = 1000000
DATA = {'username': 'developer', 'password': '*****'}
HEADERS = {'Accept': 'application/json'}
MINUTOS_COLETA = 5

# Servidor do Glowroot
server = sys.argv[1]
# Agent ID do Glowroot
agentRollupId = sys.argv[2]

# Trata os parâmetros necessários para a chamada do serviço
def gerar_params(dtInicio, dtFim):
    return {
        'agent-rollup-id': f'{agentRollupId}',
        'transaction-type': 'Web',
        'from': format(dtInicio.timestamp() * 1000, '.0f'),
        'to': format(dtFim.timestamp() * 1000, '.0f')
    }

urlGlowroot = f'http://{server}:4000/backend'

urlLogin = f'{urlGlowroot}/login'
urlTransactions = f'{urlGlowroot}/transaction/throughput'
urlSlowTraces = f'{urlGlowroot}/transaction/trace-count'
urlErrors = f'{urlGlowroot}/error/trace-count'

# Inicia a sessao web
s = requests.Session()
# Login no Glowroot
r = s.post(url=urlLogin, data=json.dumps(DATA), headers=HEADERS)

data = datetime.today()

# Data de início será horário atual -MINUTOS_COLETA
tsInicio = data - timedelta(hours=0, minutes=MINUTOS_COLETA)

PARAMS = gerar_params(tsInicio, data)

# Request para o serviço de transações do Glowroot
r = s.get(url=urlTransactions, headers=HEADERS, params=PARAMS)
numTransactions = json.loads(r.text)['transactionsPerMin']
# Request para o serviço de slow traces do Glowroot
r = s.get(url=urlSlowTraces, headers=HEADERS, params=PARAMS)
numSlowTraces = r.text
# Request para o serviço de erros do Glowroot
r = s.get(url=urlErrors, headers=HEADERS, params=PARAMS)
numErrors = r.text

print('Transacoes:' + str(numTransactions) + ' ' + 'Slow_Traces:' + numSlowTraces + ' ' + 'NumeroErros:' + numErrors)

















